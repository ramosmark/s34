import express from 'express'

const users = []

const app = express()
const port = 4000

app.use(express.json())
app.use(express.urlencoded({extended: true}))

app.get('/home', (req, res) => {
    res.send('Welcome to home page')
})

app.get('/users', (req, res) => {
    res.send(JSON.stringify(users))
})

app.post('/add-user', (req, res) => {
    const reqBody = req.body
    
    if (reqBody.username && reqBody.password) {
        users.push(reqBody)
        console.log(users)
        res.send(`User ${reqBody.username} has been successfully registered.`)
    } else {
        res.send('Please input both username and password')
    }
})

app.delete('/delete-user', (req, res) => {
    const index = users.findIndex((user) => {
        return user.username === req.body.username
    })

    const deletedUser = users.splice(index, 1)

    if (deletedUser) {
        res.send(`${deletedUser[0].username} has been deleted from the database`)
    } else {
        res.send('No match in database')
    }
})


app.listen(port, () => console.log(`Server is running at port ${port}`))